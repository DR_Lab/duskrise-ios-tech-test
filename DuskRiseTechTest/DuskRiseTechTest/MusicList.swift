//
//  ContentView.swift
//  DuskRiseTechTest
//
//  Created by Sattar Falahati on 20/10/21.
//

import SwiftUI
import Kingfisher

struct MusicList: View {
    
    @ObservedObject var viewModel = MusicViewModel()
    
    @State private var presentPlayer = false
    
    let layout = [
        GridItem(.flexible(), spacing: 10),
        GridItem(.flexible(), spacing: 10),
        GridItem(.flexible(), spacing: 10)
    ]
    
    var body: some View {
        NavigationView {
            ScrollView {
                LazyVGrid(columns: layout, spacing: 10) {
                    ForEach(viewModel.results, id: \.self) { item in
                        VStack {
                            
                            KFImage(URL(string: item.artworkUrl100))
                                .resizable()
                                .scaledToFill()
                                .cornerRadius(10)
                            
                            Text(item.artistName)
                                .font(.body)
                                .bold()
                            
                            Text(item.name)
                                .font(.caption)
                        }
                        .onTapGesture {
                            presentPlayer.toggle()
                        }
                        .sheet(isPresented: $presentPlayer) {
                            MusicPlayer(url: item.url)
                        }
                    }
                }.padding()
                
            }
            .navigationTitle("Play List")
        }
    }
}

struct MusicList_Previews: PreviewProvider {
    static var previews: some View {
        MusicList()
    }
}

class MusicViewModel: ObservableObject {
    
    @Published var results = [Results]()
    
    init() {
        
        guard let url = URL(string: "https://rss.applemarketingtools.com/api/v2/us/music/most-played/30/songs.json") else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            guard let data = data else { return }
            do {
                let musics = try JSONDecoder().decode(Music.self, from: data)
                self.results = musics.feed.results
            }
            catch {
                print("Faild: \(error)")
            }
        }.resume()
        
        
    }
}

class Music: Decodable {
    let feed: Feeds
}

class Feeds: Decodable {
    let results: [Results]
}

struct Results: Decodable, Hashable {
    let artistName, name, artworkUrl100, url: String
}
