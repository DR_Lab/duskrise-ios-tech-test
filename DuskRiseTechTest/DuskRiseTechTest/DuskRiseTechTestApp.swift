//
//  DuskRiseTechTestApp.swift
//  DuskRiseTechTest
//
//  Created by Sattar Falahati on 20/10/21.
//

import SwiftUI

@main
struct DuskRiseTechTestApp: App {
    var body: some Scene {
        WindowGroup {
            MusicList()
        }
    }
}
