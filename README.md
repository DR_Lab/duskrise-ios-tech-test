# DuskRise iOS Tech Test


**Welcome to the DuskRise iOS Tech Test**

Here you can find a fully-operational app 🧐.

It contains bugs, can crash unexpectedly, and has been built purposely avoiding best practices 🤪.
We want to use these “imperfections” to spark a discussion on the implementation of best practices, bug fixing and code improvements.

Please make sure to go over it before your interview, prepare a list of key issues/drawbacks you have noticed, and come up with how you would prioritize fixing them.

No need to improve the code or fix bugs in advance, we will do it together during the interview session 😉.
